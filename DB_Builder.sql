USE master 

DROP DATABASE IF EXISTS InvoicingManager
CREATE DATABASE InvoicingManager
GO

USE InvoicingManager
GO

CREATE SCHEMA masterdata
GO
CREATE SCHEMA entries
GO

CREATE TABLE masterdata.country(
	ID int IDENTITY(1,1) PRIMARY KEY,
	cty_name VARCHAR(50) NOT NULL UNIQUE,
	cty_short VARCHAR(5) NOT NULL UNIQUE,
	cty_lclCurr VARCHAR(5) NOT NULL,
	cty_lclCurrRatio DECIMAL NOT NULL DEFAULT 1,
	cty_vatregPre VARCHAR(3) NULL,
	_isEU bit NOT NULL,
	_isNA bit NOT NULL,
	CHECK(
			(_isEU = 1 And _isNA = 0) 
		or (_isEU = 0 And _isNA = 1))
)

INSERT INTO masterdata.country(cty_name, cty_short, cty_lclCurr, cty_vatregPre, _isEU, _isNA) 
VALUES
('Poland', 'POL', 'PLN', 'PL', 1, 0),
('United States if America', 'USA', 'USD', NULL, 0, 1),
('Germany','GER', 'EUR', 'DE', 1, 0),
('Netherlands','NED', 'EUR', 'NE', 1, 0),
('Great Britain','GBR', 'GBP', 'GB', 1, 0)

CREATE TABLE masterdata.entity(
	ID int IDENTITY(1000,100) PRIMARY KEY,
	en_name VARCHAR(50) UNIQUE,
	en_short VARCHAR(10) UNIQUE,
	en_vatreg int NULL UNIQUE,
	en_country int FOREIGN KEY REFERENCES masterdata.country(ID)
		ON DELETE CASCADE,
	en_city VARCHAR(20),
	en_street VARCHAR(30),
	en_streetNo int,
	en_streetNoSuf char NULL,
	en_baseCurr VARCHAR(5)
)

INSERT INTO masterdata.entity(en_name,en_short,en_vatreg, en_country, en_city, en_street, en_streetNo, en_streetNoSuf, en_baseCurr) 
VALUES
('OBC USA Inc.', 'OBC USA', NULL, 2, 'New York City', '44th St', 28, NULL, 'USD')

CREATE TABLE masterdata.businessPartner(
	ID int IDENTITY(10000,100) PRIMARY KEY,
	bp_name VARCHAR(20) NOT NULL,
	bp_short VARCHAR(10) NOT NULL,
	bp_entity int FOREIGN KEY REFERENCES masterdata.entity(ID),
	bp_vatreg int NULL UNIQUE,
	bp_pmtTerms_Days int NOT NULL DEFAULT 45 CHECK(bp_pmtTerms_Days >= 0),
)

CREATE TABLE masterdata.employee(
	ID int IDENTITY(1,1) PRIMARY KEY,
	em_identifier VARCHAR(10) NOT NULL UNIQUE,
	em_name VARCHAR(20) NOT NULL,
	em_surname VARCHAR(20) NOT NULL,
	em_entity int FOREIGN KEY REFERENCES masterdata.entity(ID)
		ON DELETE CASCADE,
	em_startDate DATE NOT NULL DEFAULT CURRENT_TIMESTAMP,
	em_endDate DATE NOT NULL DEFAULT '31.12.9999',
	em_fte DECIMAL NOT NULL DEFAULT 1,
	em_baseSalary MONEY NOT NULL,
	em_cumWE_Days int NOT NULL DEFAULT 0 CHECK(em_cumWE_Days >= 0),
	em_vacationDays_Yearly int NOT NULL CHECK(em_vacationDays_Yearly >= 0),
	_active bit NOT NULL DEFAULT 1,
	_contractor bit NOT NULL DEFAULT 0
)


CREATE TABLE masterdata.wbs(
	ID int IDENTITY(1,1) PRIMARY KEY,
	wbs_code VARCHAR(20) NOT NULL UNIQUE,
	wbs_name VARCHAR(50) NOT NULL,
	wbs_partner int FOREIGN KEY REFERENCES masterdata.businessPartner(ID),
	wbs_budgetedRevenue MONEY,
	wbs_budgetedCost MONEY,

	wbs_allowance MONEY CHECK(wbs_allowance >=0),
	wbs_startDate DATE NOT NULL,
	wbs_endDate DATE NOT NULL,
	wbs_milestoneNo int NOT NULL DEFAULT 1 CHECK(wbs_milestoneNo >= 1),
	_active bit DEFAULT 1,
	_internal bit DEFAULT 0
)

CREATE TABLE entries.timesheet(
	ID int IDENTITY(1,1) PRIMARY KEY,
	ts_employee int NOT NULL FOREIGN KEY REFERENCES masterdata.employee(ID)
		ON DELETE CASCADE,
	ts_wbs int NOT NULL FOREIGN KEY REFERENCES masterdata.wbs(ID)
		ON DELETE CASCADE,
	ts_day DATE NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ts_mh decimal NOT NULL DEFAULT 8,
	ts_entryTime DATETIME DEFAULT CURRENT_TIMESTAMP,
	_approved bit NOT NULL DEFAULT 0
)
